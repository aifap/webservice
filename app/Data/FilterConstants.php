<?php
namespace App\Data;

class FilterConstants {
    const GENDER_SOLO_WOMEN = 'solo-women';
    const GENDER_SOLO_MEN = 'solo-men';
    const GENDER_STRAIGHT = 'straight';
    const GENDER_GAY = 'gay';
    const GENDER_LESBIAN = 'lesbian';
    const GENDER_TRANSEXUAL = 'transexual';
    const GENDER_UNKNOWN = 'unknown';
    
    const MEDIA_IMAGE = 'image';
    const MEDIA_ANIMATED = 'animated';
    const MEDIA_VIDEO = 'video';
    const MEDIA_AUDIO = 'audio';
    const MEDIA_TEXT = 'text';
    
    const ARTSTYLE_REALLIFE = 'real-life';
    const ARTSTYLE_HENTAI = 'hentai';
    const ARTSTYLE_3D = '3d';
    
    
    
    const ALL_GENDER = [
        self::GENDER_SOLO_WOMEN,
        self::GENDER_SOLO_MEN,
        self::GENDER_STRAIGHT,
        self::GENDER_GAY,
        self::GENDER_LESBIAN,
        self::GENDER_TRANSEXUAL,
        self::GENDER_UNKNOWN,
    ];
    
    const ALL_MEDIA = [
        self::MEDIA_IMAGE,
        self::MEDIA_ANIMATED,
        self::MEDIA_VIDEO,
        self::MEDIA_AUDIO,
        self::MEDIA_TEXT,
    ];
    
    const ALL_ARTSTYLE = [
        self::ARTSTYLE_REALLIFE,
        self::ARTSTYLE_HENTAI,
        self::ARTSTYLE_3D,
    ];
        
}
    