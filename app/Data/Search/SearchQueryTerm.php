<?php
namespace App\Data\Search;

class SearchQueryTerm {
    const TYPE_TAG = 'tag';
    const TYPE_SOURCE = 'source';
    const TYPE_TITLE = 'title';
    const TYPE_META = 'meta';
    
    public $type;
    public $ns = null;
    public $key;
    public $negate = false;
    
    public function __construct($type, $key, $ns = null, $negate = false) {
        $this->type = $type;
        $this->key = $key;
        $this->ns = $ns;
        $this->negate = $negate;
    }
}