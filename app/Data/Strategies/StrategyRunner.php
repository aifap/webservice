<?php
namespace App\Data\Strategies;

use App\User;
use App\Post;
use App\RecommendationSet;
use App\RecommendationResult;

class StrategyRunner {
    protected $strategies = [];
    
    public function addStrategy($strategy) {
        $this->strategies[] = $strategy;
    }
    
    public function recommend(User $user) {
        $now = new \Carbon\Carbon;
        
        foreach ($this->strategies as $strat) {
            $recSet = new RecommendationSet;
            $recSet->strategy_name = $strat->getName();
            $recSet->user()->associate($user);
            $recSet->save();
            
            $recs = $strat->recommend($user);
            
            $results = [];
            foreach ($recs as $postId => $score) {
                $results[] = [
                    'set_id' => $recSet->id,
                    'post_id' => $postId,
                    'score' => $score,
                    'created_at' => $now,
                    'updated_at' => $now,
                ];
            }
            
            RecommendationResult::insert($results);
        }
        
        $user->recommendationSets()->where('created_at', '<', new \Carbon\Carbon('-7 days'))->delete();
    }
}