<?php
namespace App\Data;

use App\Data\FilterConstants;
use App\Post;
use App\Subreddit;

class DuplicateChecker {
    public function isDuplicate(Post $post) {
        return !$post->is_private && $this->isDuplicateMediaUrl($post);
    }
    
    private function isDuplicateMediaUrl($post) {
        if (!$post->media_url) {
            return false;
        }
        if (!$post->id) {
            return Post::where('media_url', $post->media_url)->exists();
        }
        return Post::whereNot('id', $post->id)->where('media_url', $post->media_url)->exists();
    }
    
    public function getAllDuplicates() {
        $results = Post::select('media_url', \DB::raw('COUNT(*) as count'), \DB::raw('GROUP_CONCAT(id) as ids'))
            ->where('media_url', '!=', '')
            ->whereNotNull('media_url')
            ->where('is_private', false)
            ->groupBy('media_url')
            ->having('count', '>',  1)
            ->get();
            
        $r = [];
        foreach($results as $result) {
            $r[] = Post::whereIn('id', explode(',', $result->ids))->get();
        }
        
        return $r;
    }
    
    public function mergeNewPost(Post $newPost) {
        $oldPost = Post::where('media_url', $newPost->media_url)->first();
        
        if ($oldPost->type_gender == FilterConstants::GENDER_UNKNOWN
            && $newPost->type_gender != FilterConstants::GENDER_UNKNOWN) {
            $oldPost->type_gender = $newPost->type_gender;
        }
        
        $oldPost->save();
        return $oldPost;
    }
    
    public function mergeAndRemove(Post $original, Post $duplicate) {
        $duplicate->views()->update(['post_id' => $original->id]);
        $duplicate->ratings()->update(['post_id' => $original->id]);
        $duplicate->flags()->update(['post_id' => $original->id]);
        $duplicate->watchLaterEntries()->update(['post_id' => $original->id]);
        $duplicate->recommendations()->update(['post_id' => $original->id]);
        
        $duplicate->tags()
            ->toBase()
            ->whereNotIn('tag_id', $original->tags()->pluck('tag_id'))
            ->update(['post_id' => $original->id]);
            
        $duplicate->delete();
    }
}