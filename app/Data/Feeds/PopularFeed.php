<?php
namespace App\Data\Feeds;

use App\Data\Feeds\Feed;
use App\Data\Feeds\LiveFeed;
use App\Services\RatingsService;

use App\Post;
use App\Recommendation;

class PopularFeed extends BatchedFeed {
    public function preparePrevPage() {
        // ignore
    }
    public function prepareNextPage() {
        $this->generateRecommendations();
    }
    
    public function refresh() {
        $this->session->page = 1;
        $this->session->page_size = 50;
        $this->session->position = 0;
        $this->session->beforePost()->associate(null);
        $this->session->afterPost()->associate(null);
        $this->session->refreshed_at = new \Carbon\Carbon;
        
        $params = $this->session->params;
        $params['timestamp'] = new \Carbon\Carbon('-7 days');
        
        if (!isset($params['show_viewed'])) {
            $params['show_viewed'] = true;
        }
        
        $params['start_view_id'] = null;
        $latestView = $this->session->user->views()->orderBy('id', 'desc')->first();
        if ($latestView) {
            $params['start_view_id'] = $latestView->id;
        }
        $this->session->params = $params;
        $this->session->save();
        
        $this->cleanRecommendations();
        $this->generateRecommendations();
    }
    
    private function cleanRecommendations() {
        $this->session->recommendations()->delete();
    }
    
    
    private function generateRecommendations() {
        $q = Post::query();
        
        Feed::basePostsQueryTopLevel(
            $q,
            null,
            $this->session->getFilters(),
            null,
            $this->session->user);
            
        $skip = (($this->session->page - 1) * $this->session->page_size);
        $timestamp = $this->session->params['timestamp'];
      //  dd(\App\PostRating::Where('created_at', '>', new \Carbon\Carbon('-3 days'))->whereHas('post', function($q){$q->where('is_private', false);})->get());
      
        if (is_array($timestamp)) {
            $timestamp = $timestamp['date'];
        }
            
        $posts = RatingsService::getPopularPosts($q, $this->session->page_size, $skip, new \Carbon\Carbon($timestamp));
            
        foreach ($posts as $post) {
            $recommendation = new Recommendation;
            $recommendation->user()->associate($this->session->user);
            $recommendation->session()->associate($this->session);
            $recommendation->post()->associate($post);
            $recommendation->score = $post->score;
            $recommendation->save();
        }
        
        return $posts;
    }
    
    
    private function baseQuery() {
        $afterView = null;
        if ($this->session->after_post_id) {
            $afterView = $this->session->user->views()
                ->where('post_id', $this->session->after_post_id)
                ->first();
        }
        
        $q = $this->session->recommendations()
            ->whereHas('post', function($q2) {
                Feed::basePostsQueryTopLevel($q2, null, $this->session->getFilters(), null, $this->session->user);
            })
            ->with(['post.ratings' => function ($q) {
                $q->where('user_id', $this->session->user_id);
            }])
            ->with(['post.watchLaterEntries' => function ($q) {
                $q->where('user_id', $this->session->user_id);
            }])
            ->with(['post.recommendations' => function ($q) {
                $q->where('user_id', $this->session->user_id);
            }])
            ->with(['post.views' => function ($q) {
                $q->where('user_id', $this->session->user_id);
            }])
            ->with('post.tags')
            ->with('post.tags.tagNamespace');
            
        if (!isset($this->session->getParams()['show_viewed'])
            || !$this->session->getParams()['show_viewed']) {
            $q->whereDoesntHave('post.views', function($q2) {
                $q2->where('user_id', $this->session->user->id);
                if (isset($this->session->getParams()['start_view_id'])) {
                    $q2->where('id', '<=', $this->session->getParams()['start_view_id']);
                }
            });
        }
        
        return $q;
    }
    
    public function getItems() {
        return $this->baseQuery()
            ->orderBy('id', 'asc')
            ->skip(($this->session->page - 1) * $this->session->page_size)
            ->take($this->session->page_size)
            ->get()
            ->pluck('post');
    }
    
    public function getFirstItem() {
        return $this->baseQuery()
            ->orderBy('id', 'desc')
            ->first()
            ->post;
    }
    
    public function getLastItem() {
        return $this->baseQuery()
            ->orderBy('id', 'asc')
            ->first()
            ->post;
    }
}