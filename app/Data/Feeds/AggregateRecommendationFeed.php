<?php
namespace App\Data\Feeds;

use App\Data\Feeds\Feed;
use App\Data\Feeds\LiveFeed;

use App\Services\RatingsService;
use App\Services\RecommendationsServiceV3;

use App\Post;
use App\Recommendation;
use App\RecommendationSet;
use App\RecommendationResult;

class AggregateRecommendationFeed extends LiveFeed {
    protected $recommendationLevel;
    protected $seed1;
    protected $seed2;
    protected $startViewId;
    protected $setWeights;
    protected $includeStrats;
    
    public function setup() {
        $now = new \Carbon\Carbon;
        
        // Rec sets
        $this->setWeights = $this->session->param('set_weights', function() {
            return RecommendationsServiceV3::getActiveRecommendationSets($this->session->user);
        });
        
        // Offsets
        $this->startViewId = $this->session->param('start_view_id', function() {
            $latestView = $this->session->user->views()->orderBy('id', 'desc')->first();
            return $latestView ? $latestView->id : null;
        });
        
        $this->includedStrats = $this->session->param('included_strats', function() {
            return [
                'CollabUserV3',
                'ContentNewV1',
            ];
        });
        
        if (count($this->includedStrats) < 1) {
            $this->session->setParam('included_strats', [
                'CollabUserV3',
                'ContentNewV1',
            ]);
        }
        
        $this->session->save();
    }
    
    public function refresh() {
        if (RecommendationsServiceV3::needsRecommendations($this->session->user)) {
            RecommendationsServiceV3::recommendUser($this->session->user);
        }
        
        $this->session->page = 1;
        $this->session->page_size = 50;
        $this->session->position = 0;
        $this->session->beforePost()->associate(null);
        $this->session->afterPost()->associate(null);
        $this->session->refreshed_at = new \Carbon\Carbon;
        
        $this->session->error = null;
        $this->session->resetParams([
            'set_weights',
            'start_view_id',
        ]);
        $this->setup();
    }
    
    
    private function baseQuery() {
        $includedSetIds = [];
        foreach (array_keys($this->setWeights) as $setId) {
            $set = RecommendationSet::find($setId);
            if (in_array($set->strategy_name, $this->includedStrats)) {
                $includedSetIds[] = $setId;
            }
        }
        $query = RecommendationResult::query()
            ->whereIn('set_id', $includedSetIds)
            ->with('recommendationSet')
            ->where(function($q) {
                $q->whereNull('viewed_at');
                if ($this->session->refreshed_at) {
                    $q->orWhere('viewed_at', '>', $this->session->refreshed_at);
                }
            })
            ->whereHas('post', function($q2) {
                Feed::basePostsQueryTopLevel($q2, null, $this->session->getFilters(), null,
                    $this->session->user, true, false);
                $q2->where('is_private', false);
                /*
                    ->whereDoesntHave('views',function($q3) {
                        $q3->where('user_id', $this->session->user_id)
                            ->where('id', '<=', $this->startViewId);
                    });
                */
            })
            ->with(['post.ratings' => function ($q) {
                $q->where('user_id', $this->session->user_id);
            }])
            ->with(['post.watchLaterEntries' => function ($q) {
                $q->where('user_id', $this->session->user_id);
            }])
            ->with(['post.views' => function ($q) {
                $q->where('user_id', $this->session->user_id);
            }])
            ->with('post.tags')
            ->with('post.tags.tagNamespace');
            
        return $query;
    }
    
    public function getItems() {
        $recResults = $this->baseQuery()
            ->orderBy('score', 'desc')
            ->skip(($this->session->page - 1) * $this->session->page_size)
            ->take($this->session->page_size)
            ->get();
        
        $posts = [];
        foreach ($recResults as $recResult) {
            $post = $recResult->post;
            unset($recResult->post);
            $post->recommendation_results = [$recResult];
            $posts[] = $post;
        }
        return collect($posts);
    }
    
    public function getFirstItem() {
        return $this->baseQuery()
            ->orderBy('score', 'asc')
            ->first()
            ->post;
    }
    
    public function getLastItem() {
        return $this->baseQuery()
            ->orderBy('score', 'desc')
            ->first()
            ->post;
    }
}