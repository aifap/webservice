<?php
namespace App\Data\Feeds;

use App\Data\Feeds\Feed;
use App\Data\Feeds\LiveFeed;

use App\Post;
use App\User;
use App\SavedList;

class SavedListFeed extends LiveFeed {
    protected $listId;
    protected $list;
    
    public function setup() {
        $now = new \Carbon\Carbon;
        
        $this->listId = $this->session->param('saved_list_id', -1);
        
        $list = SavedList::find($this->listId);
        if (!$list) {
            $this->session->error = 'List not specified.';
        } else if (!$list->is_public && $list->user_id != $this->session->user->id) {
            $this->session->error = 'Unauthorized';
        } else {
            $this->list = $list;
        }
        
        $this->session->save();
    }
    
    public function refresh() {
        $this->session->page = 1;
        $this->session->page_size = 50;
        $this->session->position = 0;
        $this->session->beforePost()->associate(null);
        $this->session->afterPost()->associate(null);
        $this->session->refreshed_at = new \Carbon\Carbon;
        
        $this->session->error = null;
        $this->session->resetParams([
        ]);
        $this->setup();
        
    }
    
    
    private function baseQuery() {
        return $this->list->posts()
            ->with(['ratings' => function ($q) {
                $q->where('user_id', $this->session->user_id);
            }])
            ->with(['watchLaterEntries' => function ($q) {
                $q->where('user_id', $this->session->user_id);
            }])
            ->with(['views' => function ($q) {
                $q->where('user_id', $this->session->user_id);
            }])
            ->with('tags')
            ->with('tags.tagNamespace');
            
        return $query;
    }
    
    public function getItems() {
        if (!$this->list) {
            return [];
        }
        
        $posts = $this->baseQuery()
            ->orderBy('saved_list_posts.id', 'desc')
            ->skip(($this->session->page - 1) * $this->session->page_size)
            ->take($this->session->page_size)
            ->get();
            
        return $posts;
    }
    
    public function getFirstItem() {
        if (!$this->list) {
            return null;
        }
        
        return $this->baseQuery()
            ->orderBy('saved_list_posts.id', 'asc')
            ->first();
    }
    
    public function getLastItem() {
        if (!$this->list) {
            return null;
        }
        
        return $this->baseQuery()
            ->orderBy('saved_list_posts.id', 'desc')
            ->first();
    }
}