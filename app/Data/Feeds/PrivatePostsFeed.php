<?php
namespace App\Data\Feeds;

use App\Data\Feeds\Feed;
use App\Data\Feeds\LiveFeed;

use App\Post;

class PrivatePostsFeed extends LiveFeed {
    const SORT_DEFAULT = 'default';
    const SORT_SHUFFLE = 'shuffle';
    
    protected $sort;
    protected $seed1;
    
    public function setup() {
        $now = new \Carbon\Carbon;
        
        $this->seed1 = intval($this->session->param('seed1', $now->timestamp));
        $this->sort = $this->session->param('sort', self::SORT_DEFAULT);
        
        $this->session->save();
    }
    
    public function refresh() {
        parent::refresh();
        
        $this->session->error = null;
        $this->session->resetParams([
            'seed1',
        ]);
        $this->setup();
        
        $this->session->afterPost()->associate($this->getLastItem());
    }
    
    private function baseQuery() {
        return Feed::basePostsQuery(
            $this->session->user,
            $this->session->after_post_id, 
            $this->session->getFilters(),
            null,
            true)
            ->where('is_private', true);
    }
    
    public function getItems() {
        $q = $this->baseQuery();
        
        if ($this->sort == self::SORT_SHUFFLE) {
            $q->orderBy(\DB::raw('RAND(' . $this->seed1 . ')', 'desc'));
        } else {
            $q->orderBy('id', 'desc');
        }
        
        return $q
            ->orderBy('id', 'desc')
            ->skip(($this->session->page - 1) * $this->session->page_size)
            ->take($this->session->page_size)
            ->get();
    }
    
    public function getFirstItem() {
        $q = $this->baseQuery();
        if ($this->sort == self::SORT_SHUFFLE) {
            $q->orderBy(\DB::raw('RAND(' . $this->seed1 . ')', 'desc'));
        } else {
            $q->orderBy('id', 'asc');
        }
        
        return $q->first();
    }
    
    public function getLastItem() {
        $q = $this->baseQuery();
        if ($this->sort == self::SORT_SHUFFLE) {
            $q->orderBy(\DB::raw('RAND(' . $this->seed1 . ')', 'asc'));
        } else {
            $q->orderBy('id', 'desc');
        }
        
        return $q->first();
    }
}