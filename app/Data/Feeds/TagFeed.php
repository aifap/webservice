<?php
namespace App\Data\Feeds;

use App\Data\Feeds\Feed;
use App\Data\Feeds\LiveFeed;

use sngrl\SphinxSearch\SphinxSearch;

use App\Post;

class TagFeed extends LiveFeed {
    public function refresh() {
        parent::refresh();
    }
        
    private function sphinxEscape($string) {
        $from = array ( '\\', '(',')','|','-','!','@','~','"','&', '/', '^', '$', '=' );
        $to = array ( '\\\\', '\(','\)','\|','\-','\!','\@','\~','\"', '\&', '\/', '\^', '\$', '\=' );
        return str_replace ( $from, $to, $string );
    }

    
    public function getItems() {
        $sphinx = new SphinxSearch();
        
        $tagId = $this->session->getParams()['tag_id'];
        $tagId = $this->sphinxEscape($tagId);
        
        $cleanup = function($arr) {
            return array_map(function($v) {
                return '"' . $v . '"';
            }, $arr);
        };
        
        $filters = $this->session->getFilters();
        $genderTypes = $cleanup($filters['gender_type']);
        $mediaTypes = $cleanup($filters['media_type']);
        $artstyleTypes = $cleanup($filters['artstyle_type']);
        
        $query = '@tags ' . $tagId;
        $query .= '@type_gender (' . implode(' | ', $genderTypes) . ')'; 
        $query .= ' @type_media (' . implode(' | ', $mediaTypes) . ')';
        $query .= ' @type_artstyle (' . implode(' | ', $artstyleTypes) . ')';
       
        $postIds = collect($sphinx->search($query, 'test1')
        	->setMatchMode(\Sphinx\SphinxClient::SPH_MATCH_EXTENDED)
        	->setFieldWeights(['tags' => 1, 'type_gender' => 0, 'type_media' => 0, 'type_artstyle' => 0])
        	->setSortMode(\Sphinx\SphinxClient::SPH_SORT_EXTENDED, "@id DESC")
            ->limit($this->session->page_size, ($this->session->page - 1) * $this->session->page_size)
            ->get())
            ->pluck('id');
            
        \Log::info('tagsearch', [
            'query' => $query,
            'postIds' => $postIds,
        ]);
        
        $q = Post::orderBy('id', 'desc');
        Feed::basePostsQueryRelations($q, $this->session->user);
        $q->aboveFlagThreshold();
        $q->whereIn('id', $postIds);
        
        $posts = $q->get();
        
        \Log::info('tagsearch', [
            'postIds_after' => $posts->pluck('id'),
        ]);
        
        return $posts;
    }
    
    public function getFirstItem() {
        return null;
    }
    
    public function getLastItem() {
        return null;
    }
}