<?php
namespace App\Data\Feeds;

use App\FeedSession;
use App\Post;

abstract class Feed {
    public $session;
    public $viewingId;
    
    public function __construct(FeedSession $session) {
        $this->session = $session;
        $this->setup();
    }
    
    public function setup() {
    }
    
    public function firstPage() {
        $this->session->page = 1;
        $this->session->position = 0;
        return $this->getItems();
    }
    
    public function nextPage() {
        $this->session->page += 1;
        
        $this->prepareNextPage();
        $this->session->position = 0;
        return $this->getItems();
    }
    
    public function prevPage() {
        $this->session->page -= 1;
        
        $this->preparePrevPage();
        $items = $this->getItems();
        $this->session->position = count($items) - 1;
        return $items;
    }
    
    
    /**
     * Return the session to the start of the feed, and include any new items
     */
    public abstract function refresh();
    
    
    /**
     * Do any preparation required to get the next page of results.
     */
    public abstract function prepareNextPage();
    
    /**
     * Do any preparation required to get the prev page of results.
     */
    public abstract function preparePrevPage();
    
    /**
     * Return the current page of results and update the feed session.
     */
    public abstract function getItems();
    
    
    /**
     * Return the first post in the feed. (the item at the bottom)
     */
    public abstract function getFirstItem();
    /**
     * Return the last post in the feed. (the item at the top)
     */
    public abstract function getLastItem();
    
    public static function basePostsQueryTopLevel($q, $after, $filters, $subreddit, $user, $includePrivate = false, $includeUserPrivate = true) {
        $filterGenderType = $filters['gender_type'];
        $filterMediaType = $filters['media_type'];
        $filterArtstyleType = $filters['artstyle_type'];
        
        $q->notRemoved()
            ->aboveFlagThreshold()
            ->whereIn('type_gender', $filterGenderType)
            ->whereIn('type_media', $filterMediaType)
            ->whereIn('type_artstyle', $filterArtstyleType);
            
        if ($after) {
            $q->where('id', '<=', $after);
        }
        
        if ($subreddit) {
            $q->where('subreddit_id', $subreddit);
        }
            
        if (!$includePrivate) {
           $q->where('is_private', false);
        } else if ($includeUserPrivate) {
            $q->visibleToUser($user);
        }
    }
    
    public static function basePostsQueryRelations($q, $user) {
        $q->with(['ratings' => function ($q) use ($user) {
                $q->where('user_id', $user->id);
        }])
        ->with(['watchLaterEntries' => function ($q) use ($user) {
            $q->where('user_id', $user->id);
        }])
        ->with(['views' => function ($q) use ($user) {
            $q->where('user_id', $user->id);
        }])
        ->with('tags');
    }
    
    public static function basePostsQuery($user, $after, $filters, $subreddit, $includePrivate = false, $includeUserPrivate = true) {
        $q = Post::query();
        self::basePostsQueryTopLevel($q, $after, $filters, $subreddit, $user, $includePrivate, $includeUserPrivate);
        self::basePostsQueryRelations($q, $user);
        return $q;
    }
}