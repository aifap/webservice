<?php
namespace App\Data\Feeds;

use App\Data\Feeds\Feed;
use App\FeedSession;

/**
 * A feed which doesn't require pre-batching items. i.e. new posts
 */
abstract class LiveFeed extends Feed {
    public function preparePrevPage() {
        // live feeds need no preparation
    }
    public function prepareNextPage() {
        // live feeds need no preparation
    }
    
    public function refresh() {
        // live feeds can usually skip right to the start of the feed.
        $this->session->page = 1;
        $this->session->position = 0;
        $this->session->beforePost()->associate(null);
        $this->session->afterPost()->associate(null);
        $this->session->refreshed_at = new \Carbon\Carbon;
    }
    
    public function getExcludedItemsCount() {
        return -1;
    }
}