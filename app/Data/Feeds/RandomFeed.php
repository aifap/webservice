<?php
namespace App\Data\Feeds;

use App\Data\Feeds\Feed;
use App\Data\Feeds\LiveFeed;

use App\Post;

class RandomFeed extends LiveFeed {
    public function refresh() {
        parent::refresh();
        $this->session->params = [
            'timestamp' => \Carbon\Carbon::now()->timestamp
        ];
    }
    
    private function baseQuery() {
        return Feed::basePostsQuery(
            $this->session->user,
            $this->session->after_post_id, 
            $this->session->getFilters(),
            null);
    }
    
    public function getItems() {
        if (!isset($this->session->getParams()['timestamp'])) {
            $this->refresh();
        }
        $timestamp = $this->session->getParams()['timestamp'];
        return $this->baseQuery()
            ->orderBy(\DB::raw('RAND(' . $timestamp . ')'), 'desc')
            ->skip(($this->session->page - 1) * $this->session->page_size)
            ->take($this->session->page_size)
            ->get();
    }
    
    public function getFirstItem() {
        if (!isset($this->session->getParams()['timestamp'])) {
            $this->refresh();
        }
        $timestamp = $this->session->getParams()['timestamp'];
        return $this->baseQuery()
            ->orderBy(\DB::raw('RAND(' . $timestamp . ')'), 'asc')
            ->first();
    }
    
    public function getLastItem() {
        if (!isset($this->session->getParams()['timestamp'])) {
            $this->refresh();
        }
        $timestamp = $this->session->getParams()['timestamp'];
        return $this->baseQuery()
            ->orderBy(\DB::raw('RAND(' . $timestamp . ')'), 'desc')
            ->first();
    }
}