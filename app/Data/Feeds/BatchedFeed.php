<?php
namespace App\Data\Feeds;

use App\Data\Feeds\Feed;

/**
 * A feed which requires pre-batching items, i.e. recommendations
 */
abstract class BatchedFeed extends Feed {
}
