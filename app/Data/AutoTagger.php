<?php
namespace App\Data;

use App\Subreddit;
use App\AutoTitleTag;
use App\Post;
use App\Tag;


class AutoTagger {
    protected $subredditTags;
    protected $autoTitleTags;
    
    public function __construct() {
        $subreddits = Subreddit::with('defaultTags')->get();
        $this->subredditTags = [];
        foreach ($subreddits as $subreddit) {
            $this->subredditTags[$subreddit->id] = $subreddit->defaultTags->pluck('id')->toArray();
        }
        
        $this->autoTitleTags = AutoTitleTag::pluck('tag_id', 'pattern')->toArray();
    }
    
    public function tagPost(Post $post) {
        $tagIds = array_merge(
            $this->getSubredditTags($post->subreddit_id), 
            $this->getAutoTitleTags($post->title)
        );
        
        $post->tags()->sync($tagIds);
    }
    
    private function getSubredditTags($subredditId) {
        if (isset($this->subredditTags[$subredditId])) {
            return $this->subredditTags[$subredditId];
        }
        return [];
    }
    
    private function getAutoTitleTags($title) {
        $tagIds = [];
        foreach ($this->autoTitleTags as $pattern => $tagId) {
            $match = preg_match($pattern, $title);
            if ($match) {
                $tagIds[] = $tagId;
            }
        }
        return $tagIds;
    }
    
}
    