<?php
namespace App\Data\Progression;

class ProgressionLevel {
    public $name;
    public $achieved;
    public $unlocks;
    
    private $unlockKeys = [];
    
    public function __construct($name, $achieved, $unlocks) {
        $this->name = $name;
        $this->achieved = $achieved;
        $this->unlocks = $unlocks;
        
        $this->unlockKeys = [];
        foreach ($this->unlocks as $unlock) {
            $this->unlockKeys[] = $unlock->key;
        }
    }
    
    public function unlocks(ProgressionUnlock $unlock) {
        return in_array($unlock->key, $this->unlockKeys);
    }
}