<?php
namespace App\Data\Progression;

class ProgressionProfile {
    public $channels = [];
    
    public function __construct($channels) {
        $this->channels = $channels;
    }
    
    public function hasUnlocked($unlock) {
        foreach ($this->channels as $channel) {
            foreach ($channel->levels as $level) {
                if ($level->achieved && $level->unlocks($unlock)) {
                    return true;
                }
            }
        }
        return false;
    }
}