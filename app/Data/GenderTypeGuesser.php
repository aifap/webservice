<?php
namespace App\Data;

use App\Post;
use App\Subreddit;


class GenderTypeGuesser {
    static $titleStringsMale = [
        '/\[\d* ?m ?\d*\]/i', 
        '/\(\d* ?m ?\d*\)/i', 
        '/\{\d* ?m ?\d*\}/i', 
    ];
    static $titleStringsFemale = [
        '/\[\d* ?f ?\d*\]/i', 
        '/\(\d* ?f ?\d*\)/i', 
        '/\{\d* ?f ?\d*\}/i', 
    ];
    static $titleStringsTrans = [
        '/\[\d* ?t ?\d*\]/i', 
        '/\(\d* ?t ?\d*\)/i', 
        '/\{\d* ?t ?\d*\}/i', 
    ];
    static $titleStringsSolo = [
    ];
    static $titleStringsNonsolo = [
        '/threesome/',
        '/3some/'
    ];
    
    public function guess(Post $post, Subreddit $subreddit, $postData) {
        $hints = $subreddit->typeHint;
        if (!$hints) {
            return FilterConstants::GENDER_UNKNOWN;
        }
        if ($hints->fixed_gender_type) {
            return $hints->fixed_gender_type;
        }
        
        $definitelySolo = $this->titleStringMatch($post->title, static::$titleStringsSolo);
        $definitelyNonSolo = $this->titleStringMatch($post->title, static::$titleStringsNonsolo);
        
        if (($definitelySolo || $hints->always_solo) && !$definitelyNonSolo) {
            if ($this->titleStringMatch($post->title, static::$titleStringsMale)) {
                return FilterConstants::GENDER_SOLO_MEN;
            }
            if ($this->titleStringMatch($post->title, static::$titleStringsFemale)) {
                return FilterConstants::GENDER_SOLO_WOMEN;
            }
        }
        
        if (($definitelyNonSolo || $hints->never_solo) && !$definitelySolo) {
            if ($hints->always_straight) {
                return FilterConstants::GENDER_STRAIGHT;
            }
            if ($hints->always_gay) {
                return FilterConstants::GENDER_GAY;
            }
                
            if ($this->titleStringMatch($post->title, static::$titleStringsMale)) {
                if ($this->titleStringMatch($post->title, static::$titleStringsFemale)) {
                    return FilterConstants::GENDER_STRAIGHT;
                }
            }
        }
        
        if ($this->titleStringMatch($post->title, static::$titleStringsMale)
            && !$this->titleStringMatch($post->title, static::$titleStringsFemale)) {
            return FilterConstants::GENDER_SOLO_MEN;
        }
        if ($this->titleStringMatch($post->title, static::$titleStringsFemale)
            && !$this->titleStringMatch($post->title, static::$titleStringsMale)) {
            return FilterConstants::GENDER_SOLO_WOMEN;
        }
        if ($this->titleStringMatch($post->title, static::$titleStringsTrans)) {
            return FilterConstants::GENDER_TRANSEXUAL;
        }
        if ($hints->always_transexual) {
            return FilterConstants::GENDER_TRANSEXUAL;
        }
        if ($hints->always_gay) {
            return FilterConstants::GENDER_GAY;
        }
        if ($hints->always_straight) {
            return FilterConstants::GENDER_STRAIGHT;
        }
        
        return FilterConstants::GENDER_UNKNOWN;
    }
    
    
    private function titleStringMatch($title, $strings) {
        $title = strtolower($title);
        foreach ($strings as $str) {
            if (preg_match($str, $title)) {
                return true;
            }
        }
        return false;
    }
}



