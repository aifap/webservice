<?php
namespace App\Data\Reputation;

class ReputationProfile {
    public $trust = 0;
    public $levels = [];
    public $recentActions = [];
    public $actions = [];
    
    public function __construct($trust, $levels = [], $recentActions = [], $actions = []) {
        $this->trust = $trust;
        $this->levels = $levels;
        $this->recentActions = $recentActions;
        $this->actions = $actions;
    }
    
    public function hasUnlocked($unlock) {
        foreach ($this->levels as $level) {
            if ($level->achieved && $level->unlocks($unlock)) {
                return true;
            }
        }
        return false;
    }
}