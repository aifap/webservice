<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MergeDuplicatePosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'posts:merge_duplicate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $duplicateFullnames = \App\Post::select('fullname', \DB::raw('count(*)'))
            ->groupBy('fullname')
            ->havingRaw('count(*) > 1')
            ->get()
            ->pluck('fullname');
            
        dump($duplicateFullnames);
            
        foreach ($duplicateFullnames as $fullname) {
            $firstPost = \App\Post::orderBy('id', 'asc')->where('fullname', $fullname)->first();
            $duplicates = \App\Post::orderBy('id', 'asc')->where('fullname', $fullname)->limit(999999)->skip(1)->get();
            
            foreach ($duplicates as $duplicate) {
                $duplicate->ratings()->update(['post_id' => $firstPost->id]);
                $duplicate->flags()->update(['post_id' => $firstPost->id]);
                $duplicate->watchLaterEntries()->update(['post_id' => $firstPost->id]);
                $duplicate->recommendations()->update(['post_id' => $firstPost->id]);
                
                $duplicate->delete();
            }
        }
    }
}
