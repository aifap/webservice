<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class GenerateRecommendations extends Command {
    protected $signature = 'recommendations:generate';
    protected $description = 'Command description';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        $runner = new \App\Data\Strategies\StrategyRunner;
        $runner->addStrategy(new \App\Data\Strategies\CollabUserV1);
        $runner->addStrategy(new \App\Data\Strategies\ContentNewV1);
        
        foreach (\App\User::all() as $user) {
            $this->info('Recommending for : ' . $user->id .' : ' . $user->username);
            $runner->recommend($user);
        }
    }
}
