<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class TestDisplayUserProfile extends Command {
    protected $signature = 'test:display_user_profile {user}';
    protected $description = 'Command description';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        $user = \App\User::find($this->argument('user'));
        
        $q = $user->ratings()
            ->leftJoin('posts', 'post_ratings.post_id', '=', 'posts.id')
            ->select('type', 'posts.subreddit_id', \DB::raw('COUNT(*) as count'))
            ->groupBy('type', 'posts.subreddit_id')
            ->get();
            
        $ratingsMatrix = []; // sub id => rating score
        foreach ($q as $entry) {
            if (!isset($ratingsMatrix[$entry->subreddit_id])) {
                $ratingsMatrix[$entry->subreddit_id] = 0;
            }
            $score = [
                'love' => 10,
                'like' => 1,
                'dislike' => -1,
                'hate' => -10,
            ][$entry->type];
            $score *= $entry->count;
            
            $ratingsMatrix[$entry->subreddit_id] += $score;
        }
        
        dd($ratingsMatrix);
    }
}
