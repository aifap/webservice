<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Data\DuplicateChecker;

class DeduplicatePosts extends Command {
    protected $signature = 'post:deduplicate';
    protected $description = 'Command description';
    
    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        $duplicateChecker = new DuplicateChecker;
        
        $allDuplicates = $duplicateChecker->getAllDuplicates();
        $this->info('Total duplicate sets: ' . count($allDuplicates));
        
        foreach ($allDuplicates as $posts) {
            $first = $posts[0];
            foreach ($posts as $post) {
                if ($post == $first) {
                    continue;
                }
                
                $this->info('Dedupl: ' . $first->title . ' <- ' . $post->title);
                $duplicateChecker->mergeAndRemove($first, $post);
            }
        }
        
    }
}
