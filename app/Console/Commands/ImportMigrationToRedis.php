<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;

use League\Csv\Reader;
use League\Csv\Statement;

use Illuminate\Support\Facades\Redis;


class ImportMigrationToRedis extends Command {
    protected $signature = 'app:import_migration_to_redis';
    protected $description = 'Command description';

    public function __construct() {
        parent::__construct();
    }
    
    function getLines($file) {
        $f = fopen($file, 'rb');
        $lines = 0;
    
        while (!feof($f)) {
            $lines += substr_count(fread($f, 8192), "\n");
        }
    
        fclose($f);
    
        return $lines;
    }

    public function handle() {
        $this->importRatings();
        $this->importViews();
    }
    
    private function importViews() {
        $viewsPath = storage_path() . '/app/migrate/views.csv';
        
        $count = $this->getLines($viewsPath);
        $csvViews = Reader::createFromPath($viewsPath, 'r');
        
        
        foreach ($csvViews as $offset => $record) {
            $userId = $record[0];
            $postId = $record[1];
            $timestamp = $record[2];
            
            Redis::sadd('views:all#'.$userId, $postId);
            Redis::zadd('views:by_date#'.$userId, $postId, $timestamp);
            
            if (($offset % 10000) == 0) {
                $p = number_format(($offset / $count) * 100, 0);
                $this->info('Views: ' . number_format($offset) . ' / ' . number_format($count) . ' (' . $p . '%)');
            }
        }
        
    }
    
    private function importRatings() {
        $ratingsPath = storage_path() . '/app/migrate/ratings.csv';
        
        $count = $this->getLines($ratingsPath);
        $csvRatings = Reader::createFromPath($ratingsPath, 'r');
        
        $ratingMap = [
            '10' => 'love',
            '1' => 'like',
            '-1' => 'dislike',
            '-10' => 'hate',
        ];
        
        
        foreach ($csvRatings as $offset => $record) {
            $userId = $record[0];
            $postId = $record[1];
            $ratingScore = $record[2];
            $timestamp = $record[3];
            
            $rating = $ratingMap[$ratingScore];
            
            Redis::sadd('ratings:rated:all#'.$userId, $postId);
            Redis::zadd('ratings:rated:by_date#'.$userId, $postId, $timestamp);
            
            Redis::sadd('ratings:'.$rating.'.:all#'.$userId, $postId);
            
            if (($offset % 10000) == 0) {
                $p = number_format(($offset / $count) * 100, 0);
                $this->info('Ratings: ' . number_format($offset) . ' / ' . number_format($count) . ' (' . $p . '%)');
            }
        }
    }
}
