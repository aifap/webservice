<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class FixGfycatPosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'posts:fix_gfycat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $client = new \GuzzleHttp\Client();
        $res = $client->request('POST', 'https://api.gfycat.com/v1/oauth/token', [
            'json' => [
                'grant_type' => 'client_credentials',
                'client_id' => 'REPLACEME',
                'client_secret' => 'REPLACEME',
            ],
        ]);
        $data = json_decode($res->getBody());
        
        $gfyToken = $data->access_token;
        
        foreach (\App\Post::where('media_url', 'like', '%gfycat.com%')->where('media_url', 'not like', '%giant.gfycat.com%')->get() as $post) {
            $this->info($post->media_url);
            try {
                preg_match('/^https?:\/\/(?:.*\.)?gfycat\.com\/(?:.*\/)?(.*?)(?:\..*)?$/m', $post->media_url, $matches);
                $gfyId = $matches[1];
                $res = $client->request('GET', 'https://api.gfycat.com/v1/gfycats/' . $gfyId, [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $gfyToken,
                    ],
                ]);
                $body = $res->getBody();
                $gfyPost = json_decode($body);
                
                $post->media_url = $gfyPost->gfyItem->webmUrl;
                $post->thumbnail_url = $gfyPost->gfyItem->gif100px;
                
                $this->info(' -> ' . $post->media_url);
            } catch (\Exception $ex) {
                dump($ex);
                $contents = $ex->getResponse()->getBody()->getContents();
                $data = json_decode($contents);
                if (strpos($data->errorMessage, 'does not exist') !== false) {
                    $post->delete();
                    continue;
                }
                dd($ex);
            }
            $post->save();
        }
            
    }
}
