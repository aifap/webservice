<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Post;
use App\Tag;

class SavedList extends Model {
    protected $table = 'saved_lists';
    
    public $with = [
        'user',
    ];
    
    public $appends = [
        'topTags',
    ];
    
    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function posts() {
        return $this->belongsToMany(Post::class, 'saved_list_posts', 'list_id', 'post_id');
    }
    
    public function postsSample() {
        return $this->posts()->orderBy('saved_list_posts.id', 'desc')->take(10);
    }
    
    public function getTopTagsAttribute() {
        $tagIds = collect(\DB::select('SELECT tag_id, COUNT(*) as count
            FROM post_tags
            LEFT JOIN saved_list_posts ON post_tags.post_id = saved_list_posts.post_id
            WHERE saved_list_posts.list_id = ?
            GROUP BY tag_id
            ORDER BY count DESC
            LIMIT 10;', [$this->id]))
            ->pluck('tag_id');
            
        if (count($tagIds) < 1) {
            return collect([]);
        }
            
        return Tag::whereIn('id', $tagIds)
                  ->orderByRaw(\DB::raw("FIELD(id, ".implode(',', $tagIds->toArray()).")"))
                  ->get();
    }
}
