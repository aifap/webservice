<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\PostRating;
use App\PostView;
use App\PostFlag;
use App\WatchLaterEntry;
use App\FeedSession;
use App\Recommendation;
use App\UserSetting;
use App\Post;
use App\UserImport;
use App\UserExport;
use App\RecommendationSet;
use App\TrustEntry;
use App\SavedList;
use App\Tag;
use App\Subreddit;

class User extends Authenticatable {
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    protected $casts = [
        'gender_filter' => 'array',
        'media_filter' => 'array',
        'artstyle_filter' => 'array',
    ];
    
    
    public function watchLaterEntries() {
        return $this->hasMany(WatchLaterEntry::class, 'user_id');
    }
    
    public function ratings() {
        return $this->hasMany(PostRating::class, 'user_id');
    }
    
    public function flags() {
        return $this->hasMany(PostFlag::class, 'user_id');
    }
    
    public function views() {
        return $this->hasMany(PostView::class, 'user_id');
    }
    
    public function sessions() {
        return $this->hasMany(FeedSession::class, 'user_id');
    }
    
    public function recommendationSets() {
        return $this->hasMany(RecommendationSet::class, 'user_id');
    }
    
    public function recommendations() {
        return $this->hasMany(Recommendation::class, 'user_id');
    }
    
    public function settings() {
        return $this->hasMany(UserSetting::class, 'user_id');
    }
    
    public function imports() {
        return $this->hasMany(UserImport::class, 'user_id');
    }
    
    public function exports() {
        return $this->hasMany(UserExport::class, 'user_id');
    }
    
    public function privatePosts() {
        return $this->hasMany(Post::class, 'owner_user_id');
    }
    
    public function trustEntries() {
        return $this->hasMany(TrustEntry::class, 'user_id');
    }
    
    public function lists() {
        return $this->hasMany(SavedList::class, 'user_id');
    }
    
    public function blacklistedSubreddits() {
        return $this->belongsToMany(Subreddit::class, 'blacklisted_subreddits', 'user_id', 'subreddit_id');
    }
    
    public function blacklistedTags() {
        return $this->belongsToMany(Tag::class, 'blacklisted_tags', 'user_id', 'tag_id');
    }
    
}
