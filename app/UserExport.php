<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class UserExport extends Model {
    protected $table = 'user_exports';
    
    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
}
