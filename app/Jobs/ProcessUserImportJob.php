<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\UserImport;
use App\Services\ImportService;


class ProcessUserImportJob implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    public $import;

    public function __construct(UserImport $import) {
        $this->import = $import;
    }
    public function handle() {
        ImportService::processImport($this->import);
    }
}
