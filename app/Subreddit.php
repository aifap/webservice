<?php

namespace App;

use App\Model;

use App\TypeHint;
use App\Post;
use App\Tag;
use App\User;

class Subreddit extends Model {
    protected $table = 'subreddits';
    
    public function typeHint() {
        return $this->belongsTo(TypeHint::class, 'type_hint_id');
    }
    
    public function posts() {
        return $this->hasMany(Post::class, 'subreddit_id');
    }
    
    public function defaultTags() {
        return $this->belongsToMany(Tag::class, 'default_sub_tags', 'subreddit_id', 'tag_id');
    }
    
    public function usersBlacklisted() {
        return $this->belongsToMany(User::class, 'blacklisted_subreddits', 'subreddit_id', 'user_id');
    }
}
