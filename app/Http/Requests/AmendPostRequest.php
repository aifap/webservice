<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AmendPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type_gender' => 'in:' . implode(',', \App\Data\FilterConstants::ALL_GENDER),
            'type_artstyle' => 'in:' . implode(',', \App\Data\FilterConstants::ALL_ARTSTYLE),
        ];
    }
}
