<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTagSuggestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'new_tag_ns_id' => '',
            'new_tag_key' => 'required',
            'new_tag_description' => 'required',
            'type' => 'required|in:new-tag,new-alias',
        ];
    }
}
