<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests\FeedSessionRequest;
use App\FeedSession;
use App\Services\FeedService;

class FeedController extends Controller {
    public function refreshFeed(FeedSessionRequest $request) {
        $user = $request->user();
        $session = $request->session();
        
        $feed = FeedService::getFeed($session);
        
        $feed->refresh();
        $items = $feed->firstPage();
        $session->save();
        
        return response()->json([
            'success' => true,
            'session' => $session->fresh(),
            'posts' => $items,
        ]);
    }
    
    public function getSession(Request $request, $sessionId) {
        $user = $request->user();
        $session = $user->sessions()->where('id', $sessionId)->first();
        
        return response()->json([
            'success' => true,
            'session' => $session,
        ]);
    }
    
    public function indexFeed(FeedSessionRequest $request) {
        $user = $request->user();
        $session = $request->session();
        
        $page = $request->get('page');
        $position = $request->get('position');
        
        
        $feed = FeedService::getFeed($session);
        
        $items = null;
        if ($page) {
            $session->page = $page;
            $session->position = $position ? $position : 0;
            $items = $feed->getItems();
        } else {
            $items = $feed->firstPage();
        }
        $session->save();
        
        return response()->json([
            'success' => true,
            'session' => $session->fresh(),
            'posts' => $items,
        ]);
    }
    
    public function nextPage(FeedSessionRequest $request) {
        $user = $request->user();
        $session = $request->session();
        
        $feed = FeedService::getFeed($session);
        
        $items = $feed->nextPage();
        $session->save();
        
        
        
        return response()->json([
            'success' => true,
            'session' => $session->fresh(),
            'posts' => $items,
        ]);
    }
    
    public function prevPage(FeedSessionRequest $request) {
        $user = $request->user();
        $session = $request->session();
        
        $feed = FeedService::getFeed($session);
        
        $items = $feed->prevPage();
        $session->save();
        
        return response()->json([
            'success' => true,
            'session' => $session->fresh(),
            'posts' => $items,
        ]);
    }
}
