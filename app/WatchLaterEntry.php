<?php

namespace App;

use App\Model;

use App\User;
use App\Post;

class WatchLaterEntry extends Model {
    protected $table = 'watch_later_entries';
    
    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function post() {
        return $this->belongsTo(Post::class, 'post_id');
    }
}
