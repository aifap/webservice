<?php

namespace App;

use App\Model;
use App\Subreddit;

class TypeHint extends Model {
    protected $table = 'type_hints';
    
    public function subreddits() {
        return $this->hasMany(Subreddit::class, 'type_hint_id');
    }
}
