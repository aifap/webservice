<?php

namespace App;

use App\Model;

class PostRating extends Model {
    public function user() {
        return $this->belongsTo(\App\User::class, 'user_id');
    }
    
    public function post() {
        return $this->belongsTo(\App\Post::class, 'post_id');
    }
}