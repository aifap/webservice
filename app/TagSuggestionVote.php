<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\TagSuggestion;

class TagSuggestionVote extends Model {
    const TYPE_UPVOTE = 'upvote';
    const TYPE_DOWNVOTE = 'downvote';
    
    protected $tabe = 'tag_suggestion_votes';
    
    
    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function tagSuggestion() {
        return $this->belongsTo(TagSuggestion::class, 'tag_suggestion_id');
    }
}
