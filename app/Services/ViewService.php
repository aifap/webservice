<?php
namespace App\Services;

use Illuminate\Support\Facades\Redis;

class ViewService {
    private static function keyUserViewSet($userId) {
        return 'views:all#' . $userId;
    }
    
    private static function keyUserViewByDateSet($userId) {
        return 'views:by_date#' . $userId;
    }
    
    
    public static function recordView($userId, $postId) {
        return static::recordViewsBulk($userId, [$postId]);
    }
    
    public static function recordViewsBulk($userId, $postIds) {
        $now = (new \Carbon\Carbon)->timestamp;
        
        Redis::zadd(static::keyUserViewByDateSet($userId), array_fill_keys($postIds, $now));
        Redis::sadd(static::keyUserViewSet($userId), $postIds);
    }
}