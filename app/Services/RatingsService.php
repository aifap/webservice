<?php
namespace App\Services;

use Illuminate\Support\Facades\Redis;

use App\Post;
use App\PostRating;
use App\User;
use App\Subreddit;

use App\Data\RatingConstants;


/**
 * This service helps with processing and sorting ratings data.
 */
class RatingsService {
    private static function keyUserRatingSet($userId, $type) {
        return 'ratings:' . $type . ':all#' . $userId;
    }
    private static function keyUserRatedSet($userId) {
        return 'ratings:rated:all#' . $userId;
    }
    private static function keyUserRatedByDateSet($userId) {
        return 'ratings:rated:by_date#' . $userId;
    }
    
    public static function recordRating($userId, $postId, $type) {
        $now = (new \Carbon\Carbon)->timestamp;
        
        switch ($type){
            case 'love':
                Redis::sadd(static::keyUserRatingSet($userId, 'love'), $postId);
                Redis::srem(static::keyUserRatingSet($userId, 'like'), $postId);
                Redis::srem(static::keyUserRatingSet($userId, 'dislike'), $postId);
                Redis::srem(static::keyUserRatingSet($userId, 'hate'), $postId);
                break;
            case 'like':
                Redis::srem(static::keyUserRatingSet($userId, 'love'), $postId);
                Redis::sadd(static::keyUserRatingSet($userId, 'like'), $postId);
                Redis::srem(static::keyUserRatingSet($userId, 'dislike'), $postId);
                Redis::srem(static::keyUserRatingSet($userId, 'hate'), $postId);
                break;
            case 'dislike':
                Redis::srem(static::keyUserRatingSet($userId, 'love'), $postId);
                Redis::srem(static::keyUserRatingSet($userId, 'like'), $postId);
                Redis::sadd(static::keyUserRatingSet($userId, 'dislike'), $postId);
                Redis::srem(static::keyUserRatingSet($userId, 'hate'), $postId);
                break;
            case 'hate':
                Redis::srem(static::keyUserRatingSet($userId, 'love'), $postId);
                Redis::srem(static::keyUserRatingSet($userId, 'like'), $postId);
                Redis::srem(static::keyUserRatingSet($userId, 'dislike'), $postId);
                Redis::sadd(static::keyUserRatingSet($userId, 'hate'), $postId);
                break;
        }
        
        Redis::zadd(static::keyUserRatedByDateSet($userId), $postId, $now);
        Redis::sadd(static::keyUserRatedSet($userId), $postId);
    }
    
    
    public static function getGlobalSubScores() {
        $subs = [];
        foreach (User::all() as $user) {
            $scores = static::getSubredditRatingMatrix($user);
            foreach ($scores as $subId => $average) {
                if (!isset($subs[$subId])) {
                    $subs[$subId] = [
                        'count' => 0,
                        'score' => 0,
                    ];
                }
                $subs[$subId]['count'] += 1;
                $subs[$subId]['score'] += $average;
            }
        }
        return $subs;
    }
    
    public static function getRatingsMatrix() {
        $ratings = PostRating::selectRaw("user_id, post_id, (CASE type WHEN 'love' THEN 10 WHEN 'like' THEN 1 WHEN 'dislike' THEN -1 WHEN 'hate' THEN -10 ELSE 0 END) as score")
                    ->whereHas('post', function($q) {
                        $q->where('is_private', false);
                    })
                    ->get();

        $scoredMatrix = [];
        
//        $userIds = User::has('ratings')->pluck('id')->toArray();
//        $postIds = Post::has('ratings')->where('is_private', false)->pluck('id')->toArray();
//        $postIdIndexs = array_flip($postIds);
        
        dump('ratings: '. count($ratings));
        
        $content = '';
        foreach ($ratings as $rating) {
            $content .= "$rating->user_id,$rating->post_id,$rating->score\n";
        }
        
        file_put_contents(storage_path() . '/ratings.csv', $content);
        dd('output');
        
        $emptyArray = [];
        foreach ($postIds as $postId) {
            $emptyArray[$postId] = 0;
        }
        
        foreach ($userIds as $userId) {
            $scoredMatrix[$userId] = $emptyArray;
        }
        
        $ratings->chunk(10000, function($entries) use (&$scoredMatrix, $postIdIndexs) {
            foreach ($entries as $entry) {
                $scoredMatrix[$entry->user_id][$entry->post_id] = $entry->score;
            }
        });
        
        return $scoredMatrix;
    }
    
    public static function getPopularPosts($postsQuery, $limit, $skip, $fromTime) {
        return $postsQuery
            ->select('*')
            ->addSelect(\DB::raw('popular_feed_score as score'))
            ->orderBy('popular_feed_score', 'desc')
            ->skip($skip)
            ->take($limit)
            ->get();
        
        $ratings = PostRating::where('created_at', '>', $fromTime)
            ->select('id', 'post_id', \DB::raw("SUM(CASE WHEN type IN ('like', 'love') THEN 1 ELSE -1 END) as score"))
            ->groupBy('post_id')
            ->orderBy('score', 'desc')
            ->whereHas('post', function($q) use($postsQuery) {
                $q->union($postsQuery);
            })
            ->skip($skip)
            ->take($limit)
            ->with('post')
            ->get();
            
        $posts = [];
        foreach ($ratings as $rating) {
            if ($rating->score < 1) {
                continue;
            }
            $rating->post->score = $rating->score;
            $posts[] = $rating->post;
        }
            
        return $posts;
    }
    
    public static function getInfrequentlyRatedSubs(User $user) {
        $subIds = Subreddit::orderBy('id', 'desc')->where('slug', '!=', '_unknown')->pluck('id');
        $q = $user->ratings()
            ->whereHas('post', function($q2) {
                $q2->where('is_private', false);
            })
            ->leftJoin('posts', 'post_ratings.post_id', '=', 'posts.id')
            ->select('posts.subreddit_id', \DB::raw('COUNT(*) as count'))
            ->groupBy('posts.subreddit_id')
            ->orderBy('count', 'asc')
            ->limit(30)
            ->get()
            ->pluck('subreddit_id');
            
        $seenSubIds = $user->ratings()
            ->whereHas('post', function($q2) {
                $q2->where('is_private', false);
            })
            ->leftJoin('posts', 'post_ratings.post_id', '=', 'posts.id')
            ->select('posts.subreddit_id')
            ->distinct()
            ->pluck('subreddit_id')
            ->toArray();
        
        $r = [];
        foreach ($subIds as $id) {
            if (!in_array($id, $seenSubIds)) {
                $r[] = $id;
            }
        }
        foreach ($q as $id) {
      //      $r[] = $id;
        }
        
        return $r;
    }
    
    /**
     * Gets a ratings map of [tag_id => score] for a given user.
     */
    public static function getTagRatingMatrix(User $user) {
        $q = $user->ratings()
            ->whereHas('post', function($q2) {
                $q2->where('is_private', false);
            })
            ->rightJoin('post_tags', 'post_ratings.post_id', '=', 'post_tags.post_id')
            ->select('type', 'post_tags.tag_id', \DB::raw('COUNT(*) as count'))
            ->groupBy('type', 'post_tags.tag_id')
            ->get();
            
        $ratingsMatrix = []; // sub id => rating score
        foreach ($q as $entry) {
            if (!isset($ratingsMatrix[$entry->tag_id])) {
                $ratingsMatrix[$entry->tag_id] = [
                    'count' => 0,
                    'score' => 0,
                ];
            }
            
            $score = [
                RatingConstants::RATING_LOVE => RatingConstants::SCORE_LOVE,
                RatingConstants::RATING_LIKE => RatingConstants::SCORE_LIKE,
                RatingConstants::RATING_DISLIKE => RatingConstants::SCORE_DISLIKE,
                RatingConstants::RATING_HATE => RatingConstants::SCORE_HATE,
            ][$entry->type];
            $score *= $entry->count;
            
            $ratingsMatrix[$entry->tag_id]['score'] += $score;
            $ratingsMatrix[$entry->tag_id]['count'] += $entry->count;
        }
        
        $averageMatrix = [];
        
        foreach ($ratingsMatrix as $value => $ratings) {
            $average = $ratings['score'] / $ratings['count'];
            $averageMatrix[$value] = $average * log($ratings['count']);
        }
        
        return $averageMatrix;
    }
    
    /**
     * Gets a ratings map of [sub_id => score] for a given user.
     */
    public static function getSubredditRatingMatrix(User $user) {
        $q = $user->ratings()
            ->whereHas('post', function($q2) {
                $q2->where('is_private', false);
            })
            ->leftJoin('posts', 'post_ratings.post_id', '=', 'posts.id')
            ->select('type', 'posts.subreddit_id', \DB::raw('COUNT(*) as count'))
            ->groupBy('type', 'posts.subreddit_id')
            ->get();
            
        $ratingsMatrix = []; // sub id => rating score
        foreach ($q as $entry) {
            if (!isset($ratingsMatrix[$entry->subreddit_id])) {
                $ratingsMatrix[$entry->subreddit_id] = [
                    'count' => 0,
                    'score' => 0,
                ];
            }
            
            $score = [
                RatingConstants::RATING_LOVE => RatingConstants::SCORE_LOVE,
                RatingConstants::RATING_LIKE => RatingConstants::SCORE_LIKE,
                RatingConstants::RATING_DISLIKE => RatingConstants::SCORE_DISLIKE,
                RatingConstants::RATING_HATE => RatingConstants::SCORE_HATE,
            ][$entry->type];
            $score *= $entry->count;
            
            $ratingsMatrix[$entry->subreddit_id]['score'] += $score;
            $ratingsMatrix[$entry->subreddit_id]['count'] += $entry->count;
        }
        
        $averageMatrix = [];
        
        foreach ($ratingsMatrix as $value => $ratings) {
            $average = $ratings['score'] / $ratings['count'];
            $averageMatrix[$value] = $average * log($ratings['count']);
        }
        
        return $averageMatrix;
    }
    
    /**
     * Gets a ratings map of [gender_type|media_type|artstyle_type => [type value => score]] for a given user.
     */
    public static function getTypeRatingMatrix(User $user) {
        $q = $user->ratings()
            ->whereHas('post', function($q2) {
                $q2->where('is_private', false);
            })
            ->leftJoin('posts', 'post_ratings.post_id', '=', 'posts.id')
            ->select('type', 'posts.type_gender', 'posts.type_media', 'posts.type_artstyle', \DB::raw('COUNT(*) as count'))
            ->groupBy('type', 'posts.type_gender', 'posts.type_media', 'posts.type_artstyle')
            ->get();
            
        $ratingsMatrix = [ // type key => [type value => rating score]
            'gender_type' => [
            ],
            'media_type' => [
            ],
            'artstyle_type' => [
            ],
        ];
        foreach ($q as $entry) {
            if (!isset($ratingsMatrix['gender_type'][$entry->type_gender])) {
                $ratingsMatrix['gender_type'][$entry->type_gender] = [
                    'score' => 0,
                    'count' => 0,
                ];
            }
            if (!isset($ratingsMatrix['media_type'][$entry->type_media])) {
                $ratingsMatrix['media_type'][$entry->type_media] = [
                    'score' => 0,
                    'count' => 0,
                ];
            }
            if (!isset($ratingsMatrix['artstyle_type'][$entry->type_artstyle])) {
                $ratingsMatrix['artstyle_type'][$entry->type_artstyle] = [
                    'score' => 0,
                    'count' => 0,
                ];
            }
            
            $score = [
                RatingConstants::RATING_LOVE => RatingConstants::SCORE_LOVE,
                RatingConstants::RATING_LIKE => RatingConstants::SCORE_LIKE,
                RatingConstants::RATING_DISLIKE => RatingConstants::SCORE_DISLIKE,
                RatingConstants::RATING_HATE => RatingConstants::SCORE_HATE,
            ][$entry->type];
            
            $ratingsMatrix['gender_type'][$entry->type_gender]['score'] += $score;
            $ratingsMatrix['media_type'][$entry->type_media]['score'] += $score;
            $ratingsMatrix['artstyle_type'][$entry->type_artstyle]['score'] += $score;
            $ratingsMatrix['gender_type'][$entry->type_gender]['count'] += $entry->count;
            $ratingsMatrix['media_type'][$entry->type_media]['count'] += $entry->count;
            $ratingsMatrix['artstyle_type'][$entry->type_artstyle]['count'] += $entry->count;
        }
        
        
        $averageMatrix = [ // type key => [type value => average rating score]
            'gender_type' => [
            ],
            'media_type' => [
            ],
            'artstyle_type' => [
            ],
        ];
        
        foreach ($ratingsMatrix as $type => $values) {
            foreach ($values as $value => $ratings) {
                $average = $ratings['score'] / $ratings['count'];
                $averageMatrix[$type][$value] = $average;
            }
        }
        
        
        return $averageMatrix;
    }
    
    
}