<?php
namespace App\Services;

use GuzzleHttp\Client;

use App\Post;
use App\PostView;
use App\User;
use App\RecommendationSet;
use App\RecommendationResult;

class RecommendationsServiceV3 {
    public static function getActiveRecommendationSets(User $user) {
        $collabUserV3 = $user->recommendationSets()->where('strategy_name', 'CollabUserV3')->orderBy('id', 'desc')->first();
        $contentNewV1 = $user->recommendationSets()->where('strategy_name', 'ContentNewV1')->orderBy('id', 'desc')->first();
        
        $strats = [
        ];
        
        
        if ($contentNewV1) {
            $strats[$contentNewV1->id] = 1;
        }
        if ($collabUserV3) {
            $strats[$collabUserV3->id] = 1;
        }
        
        return $strats;
    }
    
    public static function needsRecommendations(User $user) {
        $latestSet = $user->recommendationSets()->orderBy('id', 'desc')->first();
        return !$latestSet || $latestSet->created_at->lt(new \Carbon\Carbon('-16 minutes')) || $user->id == 1;
    }
    
    public static function recommendUser(User $user) {
        $userId = $user->id;
        $baseUri = 'http://REPLACEME/';
        $client = new Client([
            'base_uri' => $baseUri,
            'timeout'  => 600,
        ]);
        $res = $client->request('GET', '/recommend/' . $userId);
        $data = json_decode((string)$res->getBody());
        
        $existingViewed = [];
        foreach (collect($data->recommendations)->chunk(1000) as $recs) {
            $viewed = PostView::select('post_id')
                ->where('user_id', $user->id)
                ->whereIn('post_id', collect($recs)->pluck('post_id'))
                ->pluck('post_id')
                ->toArray();
            $existingViewed = array_merge($existingViewed, $viewed);
        }
        
        $now = new \Carbon\Carbon;
        $recSets = [];
        $results = [];
        foreach ($data->recommendations as $rec) {
            if (!isset($recSets[$rec->strategy])) {
                $recSet = new RecommendationSet;
                $recSet->strategy_name = $rec->strategy;
                $recSet->user()->associate($user);
                $recSet->save();
                
                $recSets[$rec->strategy] = $recSet;
            }
            
            if (in_array($rec->post_id, $existingViewed)) {
                continue;
            }
            
            
            if (!Post::where('id', $rec->post_id)->exists()) {
                continue;
            }
            
            
            $results[] = [
                'set_id' => $recSets[$rec->strategy]->id,
                'post_id' => $rec->post_id,
                'score' => $rec->score,
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }
        
        RecommendationResult::insert($results);
        $user->recommendationSets()->where('created_at', '<', new \Carbon\Carbon('-2 days'))->delete();
        
    }
    
    public static function outputRecommendationsDataV2() {
        static::outputRatingsDataV2();
        static::outputPostsDataV2();
        static::outputUsersDataV2();
        //static::outputViewsDataV2();
        static::outputViewsDataV3();
    }
    
    private static function outputRatingsDataV2() {
        dump('outputting ratings v2');
        // Export to S3
        \DB::statement("
            SELECT 
                user_id, 
                post_id, 
                (CASE type WHEN 'love' THEN 10 WHEN 'like' THEN 1 WHEN 'dislike' THEN -1 WHEN 'hate' THEN -10 ELSE 0 END) as score,
                UNIX_TIMESTAMP(post_ratings.created_at) as created_at
            FROM post_ratings
            JOIN posts ON post_ratings.post_id = posts.id
            WHERE posts.is_private = 0
            LIMIT 999999999
            INTO OUTFILE S3 's3-us-east-1://REPLACEME/ratings'
            FIELDS TERMINATED BY ','
            ENCLOSED BY '\"'
            LINES TERMINATED BY '\n'
            OVERWRITE ON
        ");
    }
    
    private static function outputPostsDataV2() {
        dump('outputting posts v2');
        // Export to S3
        \DB::statement("
            SELECT 
                posts.id, 
                title, 
                type_gender, 
                type_artstyle, 
                type_media, 
                subreddit_id, 
                GROUP_CONCAT(tag_id separator '|') AS tags,
                UNIX_TIMESTAMP(posts.created_at) as created_at
            FROM posts
            LEFT JOIN post_tags ON posts.id = post_tags.post_id
            WHERE posts.is_private = 0
            GROUP BY posts.id
            LIMIT 999999999
            INTO OUTFILE S3 's3-us-east-1://REPLACEME/posts'
            FIELDS TERMINATED BY ','
            ENCLOSED BY '\"'
            LINES TERMINATED BY '\n'
            OVERWRITE ON
        ");
    }
    
    private static function outputUsersDataV2() {
        dump('outputting users v2');
        // Export to S3
        \DB::statement("
            SELECT 
                id, 
                gender_filter as _gender_filter, 
                media_filter as _media_filter, 
                artstyle_filter as _artstyle_filter,
                UNIX_TIMESTAMP(users.created_at) as created_at
            FROM users
            LIMIT 999999999
            INTO OUTFILE S3 's3-us-east-1://REPLACEME/users'
            FIELDS TERMINATED BY ','
            ENCLOSED BY '\"'
            LINES TERMINATED BY '\n'
            OVERWRITE ON
        ");
    }
    
    private static function outputViewsDataV2() {
        dump('outputting views v2');
        // Export to S3
        \DB::statement("
            SELECT 
                user_id,
                post_id,
                UNIX_TIMESTAMP(created_at) as created_at
            FROM post_views
            LIMIT 999999999
            INTO OUTFILE S3 's3-us-east-1://REPLACEME/views'
            FIELDS TERMINATED BY ','
            ENCLOSED BY '\"'
            LINES TERMINATED BY '\n'
            OVERWRITE ON
        ");
    }
    
    private static function outputViewsDataV3() {
        dump('outputting views v3');
    }
    
}