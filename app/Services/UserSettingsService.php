<?php
namespace App\Services;

use App\UserSetting;
use App\User;

class UserSettingsService {
    private static $settingsByUser = [];
    
    public static function getAll($user) {
        $settings = $user->settings->pluck('value', 'key');
        $map = [];
        foreach ($settings as $key => $value) {
            if ($value === '1') {
                $value = true;
            } else if ($value === '0') {
                $value = false;
            }
            $map[$key] = $value;
        }
        
        foreach (\Config::get('default_user_options') as $key => $default) {
            if (!isset($map[$key])) {
                $map[$key] = $default;
            }
        }
        return $map;
    }
    
    public static function setAll($user, $map) {
        foreach ($map as $key => $value) {
            static::set($user, $key, $value);
        }
    }
    
    public static function get(User $user, $key) {
        $setting = null;
        if (isset(static::$settingsByUser[$user->id]) && isset(static::$settingsByUser[$user->id][$key])) {
            return static::$settingsByUser[$user->id][$key];
        }
        $setting = $user->settings()->where('key', $key)->first();
        if ($setting) {
            if (!isset(static::$settingsByUser[$user->id])) {
                static::$settingsByUser[$user->id] = [];
            }
            static::$settingsByUser[$user->id][$key] = $setting;
        }
        
        $value = $setting ? $setting->value : static::getDefault($key);
        if ($value === '1') {
            return true;
        } else if ($value === '0') {
            return false;
        }
        return $value;
    }
    
    public static function getDefault($key) {
        return \Config::get('default_user_options.' . $key);
    }
    
    public static function set(User $user, $key, $value) {
        $setting = null;
        if (isset(static::$settingsByUser[$user->id]) && isset(static::$settingsByUser[$user->id][$key])) {
            $setting = static::$settingsByUser[$user->id][$key];
        } else {
            $setting = new UserSetting;
            $setting->user_id = $user->id;
            $setting->key = $key;
            
            if (!isset(static::$settingsByUser[$user->id])) {
                static::$settingsByUser[$user->id] = [];
            }
            static::$settingsByUser[$user->id][$key] = $setting;
        }
        $setting->value = $value;
        $setting->save();
        
        return $value;
    }
}