<?php
namespace App\Services;

use App\Post;
use App\Data\FilterConstants;

class MetadataService {
    public static function refreshMetadata(Post $post) {
        $post->has_audio_track = static::hasAudioTrack($post);
        $post->metadata_updated_at = new \Carbon\Carbon;
        $post->save();
        
        return $post;
    }
    
    
    public static function hasAudioTrack(Post $post) {
        if ($post->type_media == FilterConstants::MEDIA_VIDEO) {
            return true;
        }
        if ($post->type_media == FilterConstants::MEDIA_AUDIO) {
            return true;
        }
        if ($post->type_media == FilterConstants::MEDIA_ANIMATED) {
            return static::probeAudioTrack($post->media_url);
        }
        return false;
    }
    
    private static function probeAudioTrack($url) {
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            dd('url: ' . $url);
            return false;
        }
        if (strpos($url, '.mp4') === FALSE && strpos($url, '.webm') === FALSE) {
            return false;
        }
        exec('ffprobe -i ' . $url . ' -show_streams -select_streams a -loglevel error', $output, $result);
        
        foreach ($output as $line) {
            if ($line == 'codec_type=audio') {
                return true;
            }
        }
        return false;
    }
}