<?php
namespace App\Services;

use App\Data\Progression\ProgressionProfile;
use App\Data\Progression\ProgressionChannel;
use App\Data\Progression\ProgressionLevel;
use App\Data\Progression\ProgressionUnlock;

use App\User;

class ProgressionService {
    private static function getProgressionConfig() {
        return [
            [
                'name' => 'Ratings',
                'description' => 'Rate posts to teach us your preferences.',
                'levels' => [
                    [
                        'name' => 'Stranger',
                        'unlocks' => [
                        ],
                        'achieved' => function(User $user, $progress) {
                            return true;
                        },
                    ],
                    [
                        'name' => 'Getting started',
                        'unlocks' => [
                        ],
                        'achieved' => function(User $user, $progress) {
                            return $progress['public_ratings_count'] > 100;
                        },
                    ],
                    [
                        'name' => 'Acquaintance',
                        'unlocks' => [
                        ],
                        'achieved' => function(User $user, $progress) {
                            return $progress['public_ratings_count'] > 500;
                        },
                    ],
                    [
                        'name' => 'Friend',
                        'unlocks' => [
                        ],
                        'achieved' => function(User $user, $progress) {
                            return $progress['public_ratings_count'] > 2000;
                        },
                    ],
                ],
            ],
            [
                'name' => 'Imports',
                'description' => 'Import posts from elsewhere to build your private collection.',
                'levels' => [
                    [
                        'name' => 'Importer',
                        'unlocks' => [
                        ],
                        'achieved' => function(User $user, $progress) {
                            return $progress['import_count'] > 0;
                        },
                    ],
                ],
            ],
        ];
    }
    
    public static function getProfile(User $user) {
        $progressionData = static::getProgressionData($user);
        
        $channels = [];
        foreach (static::getProgressionConfig() as $channelData) {
            $levels = [];
            foreach ($channelData['levels'] as $levelData) {
                $unlocks = [];
                foreach ($levelData['unlocks'] as $unlockKey => $unlockName) {
                    $unlocks[] = new ProgressionUnlock(
                        $unlockKey,
                        $unlockName
                    );
                }
                $levels[] = new ProgressionLevel(
                    $levelData['name'],
                    $levelData['achieved']($user, $progressionData),
                    $unlocks
                );
            }
            $channels[] = new ProgressionChannel(
                $channelData['name'],
                $channelData['description'],
                $levels
            );
        }
        return new ProgressionProfile($channels);
    }
    
    private static function getProgressionData(User $user) {
        return [
            'ratings_count' => $user->ratings()->count(),
            'public_ratings_count' => $user->ratings()->whereHas('post', function($q) {
                $q->where('is_private', false);
            })->count(),
            'import_count' => $user->imports()->count() > 0,
        ];
    }
}