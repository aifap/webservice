<?php

use Illuminate\Database\Seeder;

use App\Data\FilterConstants;

class SubredditSeeder extends Seeder {
    public function run() {
        if (($handle = fopen(storage_path() . '/subreddits.csv', 'r')) !== false) {
            while (($data = fgetcsv($handle, 1000, ',')) !== false) {
                $typeHint = new \App\TypeHint;
                if ($data[4]) {
                    $typeHint->fixed_gender_type = $data[4] ?? null;
                }
                $typeHint->always_solo = intval($data[5]) ?? false;
                $typeHint->never_solo = intval($data[6]) ?? false;
                $typeHint->always_involve_women = intval($data[7]) ?? false;
                $typeHint->never_involve_women = intval($data[8]) ?? false;
                $typeHint->always_involve_men = intval($data[9]) ?? false;
                $typeHint->never_involve_men = intval($data[10]) ?? false;
                $typeHint->always_straight = intval($data[11]) ?? false;
                $typeHint->never_straight = intval($data[12]) ?? false;
                $typeHint->always_gay = intval($data[13]) ?? false;
                $typeHint->never_gay = intval($data[14]) ?? false;
                $typeHint->always_lesbian = intval($data[15]) ?? false;
                $typeHint->never_lesbian = intval($data[16]) ?? false;
                $typeHint->always_transexual = intval($data[17]) ?? false;
                $typeHint->never_transexual = intval($data[18]) ?? false;
                if ($data[19]) {
                    $typeHint->fixed_artstyle_type = $data[19] ?? false;
                }
                $typeHint->allows_reallife  = intval($data[20]) ?? false;
                $typeHint->bans_reallife  = intval($data[21]) ?? false;
                $typeHint->allows_hentai  = intval($data[22]) ?? false;
                $typeHint->bans_hentai  = intval($data[23]) ?? false;
                $typeHint->allows_3d  = intval($data[24]) ?? false;
                $typeHint->bans_3d  = intval($data[25]) ?? false;
                $typeHint->save();
                
                if (\App\Subreddit::where('slug', $data[1])->exists()) {
                    dump('skip: ' . $data[1]);
                    continue;
                }
                
                $sub = new \App\Subreddit;
                $sub->title = $data[0];
                $sub->slug = $data[1];
                $sub->fullname = $data[2];
                $sub->typeHint()->associate($typeHint);
                $sub->save();
                dump('created: ' . $data[1]);
         
                $tags = array_map('trim', explode(',', $data[26]));
                
                $tagIds = [];
                
                foreach ($tags as $tagString) {
                    if (!$tagString) {
                        continue;
                    }
                    
                    $ns = null;
                    $key = $tagString;
                    if (strpos($tagString, ':') !== FALSE) {
                        $parts = explode(':', $tagString);
                        $ns = $parts[0];
                        $key = $parts[1];
                    }
                    $tag = \App\Tag::namespaceString($ns)
                        ->where('key', $key)
                        ->first();
                    
                    $tagIds[] = $tag->id;
                }
                
                $sub->defaultTags()->sync($tagIds);
                dump('tagged: ' . $data[1]);
            }
        }
        
    }
}
