<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagSuggestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tag_suggestions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            
            $table->string('type');
            $table->index('type');
            
            $table->integer('score')->default(0);
            $table->index('score');
            
            $table->enum('status', [
                'pending',
                'on-hold',
                'added',
                'rejected',
            ])->default('pending');
            $table->index('status');
            
            
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('set null');
            
            $table->integer('old_tag_id')->unsigned()->nullable();
            $table->foreign('old_tag_id')
                ->references('id')
                ->on('tags')
                ->onDelete('cascade');
                
            $table->integer('new_tag_id')->unsigned()->nullable();
            $table->foreign('new_tag_id')
                ->references('id')
                ->on('tags')
                ->onDelete('cascade');
                
            $table->integer('new_tag_alias_id')->unsigned()->nullable();
            $table->foreign('new_tag_alias_id')
                ->references('id')
                ->on('tags')
                ->onDelete('cascade');
                
            $table->integer('new_tag_ns_id')->unsigned()->nullable();
            $table->foreign('new_tag_ns_id')
                ->references('id')
                ->on('tag_namespaces')
                ->onDelete('cascade');
                
                
            $table->string('new_tag_key');
            $table->string('new_tag_description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tag_suggestions');
    }
}
