<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDefaultSubTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('default_sub_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            
            $table->integer('subreddit_id')->unsigned();
            $table->foreign('subreddit_id')
                ->references('id')
                ->on('subreddits')
                ->onDelete('cascade');
                
            $table->integer('tag_id')->unsigned();
            $table->foreign('tag_id')
                ->references('id')
                ->on('tags')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('default_sub_tags');
    }
}
