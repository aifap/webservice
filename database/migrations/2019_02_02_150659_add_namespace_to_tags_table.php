<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNamespaceToTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::table('tags', function($table) {
            $table->integer('namespace_id')->unsigned()->nullable();
            $table->foreign('namespace_id')
                ->references('id')
                ->on('tag_namespaces')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('tags', function($table) {
            $table->dropColumn('namespace_id');
        });
    }
}
