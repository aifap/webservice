<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPatreonDetailsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::table('users', function($table) {
            $table->string('patreon_member_id')->nullable();
            $table->string('patreon_user_id')->nullable();
            
            $table->boolean('is_supporter')->default(false);
            $table->boolean('past_supporter')->default(false);
            $table->integer('support_amount')->nullable();
            
            $table->string('patreon_access_token')->nullable();
            $table->string('pareton_refresh_token')->nullable();
            
            $table->index('patreon_member_id');
            $table->index('patreon_user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('users', function($table) {
            $table->dropColumn([
                'patreon_member_id',
                'patreon_user_id',
                
                'is_supporter',
                'past_supporter',
                'support_amount',
                'patreon_access_token',
                'patreon_refresh_token',
            ]);
        });
    }
}
