let mix = require('laravel-mix');

mix.webpackConfig({
  resolve: {
    extensions: ['.js', '.vue', '.json', '.vue'],
    alias: {
      '@': __dirname + '/resources/assets/js',
      'sass': __dirname + '/resources/assets/sass',
    },
  },
  module: {
    rules: [
      {
        test: /\.txt$/,
        loader: 'raw-loader',
      },
    ]
  }
})

mix.js('resources/assets/js/app.js', 'public/js');
