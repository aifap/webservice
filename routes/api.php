<?php


Route::prefix('account')->group(function() {
    Route::get('/', 'AccountController@index');
    Route::post('/', 'AccountController@update');
    
    Route::post('signup', 'AccountController@signup');
    Route::post('login', 'AccountController@login');
    
    Route::get('settings', 'AccountController@getSettings');
    Route::post('settings', 'AccountController@setSettings');
    
    Route::prefix('content_preferences')->group(function() {
        Route::get('/', 'AccountController@getContentPreferences');
        Route::post('blacklist/subreddits/{subreddit}', 'AccountController@blacklistSubreddit');
        Route::post('blacklist/tags/{tag}', 'AccountController@blacklistTag');
        Route::post('blacklist/subreddits/{subreddit}/delete', 'AccountController@unblacklistSubreddit');
        Route::post('blacklist/tags/{tag}/delete', 'AccountController@unblacklistTag');
    });
    
    Route::get('patreon/connect', 'AccountController@connectPatreon');
    Route::get('patreon/connect/handle', 'AccountController@handlePatreonConnected');
    Route::post('patreon/disconnect', 'AccountController@disconnectPatreon');
    
    Route::post('change_password', 'AccountController@changePassword');
        
    Route::middleware('auth')->group(function() {
        Route::get('ratings_summary', 'AccountController@ratingsSummary');
        Route::get('account_stats', 'AccountController@accountStats');
        Route::get('progression', 'AccountController@progression');
        Route::get('trust', 'AccountController@trust');
        
        Route::post('clear_watch_later', 'AccountController@clearWatchLater');
        
        Route::get('imports', 'AccountController@getImports');
        Route::post('imports', 'AccountController@uploadImport');
        Route::post('imports/delete', 'AccountController@deleteImports');
        
        
        Route::get('exports', 'AccountController@getExports');
        Route::get('exports/{export}/download', 'AccountController@downloadExport');
        Route::post('exports', 'AccountController@startExport');
    });
});

Route::prefix('saved_lists')->middleware('auth')->group(function() {
    Route::get('/', 'SavedListController@index');
    Route::post('/', 'SavedListController@store');
    
    Route::get('context/{post}', 'SavedListController@indexContext');
    
    Route::get('{saved_list}', 'SavedListController@view');
    Route::post('{saved_list}', 'SavedListController@update');
    Route::post('{saved_list}/delete', 'SavedListController@destroy');
    
    Route::post('{saved_list}/posts/{post}', 'SavedListController@addToList');
    Route::post('{saved_list}/posts/{post}/delete', 'SavedListController@removeFromList');
});

Route::prefix('posts')->middleware('auth')->group(function() {
    Route::get('/', 'PostController@index');
    
    Route::get('search', 'PostController@search');
    
    Route::post('mark_viewed_bulk', 'PostController@viewedBulk');
    
    Route::post('{post}/amend', 'PostController@amend');
    Route::post('{post}/viewed', 'PostController@viewed');
    Route::post('{post}/rate', 'PostController@rate');
    Route::post('{post}/watchlater', 'PostController@watchLater');
    Route::post('{post}/flag', 'PostController@flag');
    Route::post('{post}/tag', 'PostController@tag');
});

Route::prefix('tags')->middleware('auth')->group(function() {
    Route::get('search', 'TagController@search');
});

Route::prefix('feeds')->middleware('auth')->group(function() {
    Route::post('index', 'FeedController@indexFeed');
    Route::post('refresh', 'FeedController@refreshFeed');
    Route::post('next', 'FeedController@nextPage');
    Route::post('prev', 'FeedController@prevPage');
    
    Route::get('sessions/{session}', 'FeedController@getSession');
});

Route::prefix('moderation')->middleware('auth')->group(function() {
    Route::get('subreddits', 'ModerationController@indexSubreddits');
    Route::post('subreddits/{subreddit}', 'ModerationController@updateSubreddit');
    
    Route::get('tags', 'ModerationController@indexTags');
    
    Route::get('amends', 'ModerationController@indexAmends');
    Route::post('amends/{user_post_amend}/revert', 'ModerationController@revertAmend');
    
    Route::get('taggings', 'ModerationController@indexTaggings');
    Route::post('taggings/{user_post_tagging}/revert', 'ModerationController@revertTagging');
});

Route::prefix('admin')->middleware('auth')->group(function() {
    Route::post('subreddits/import', 'AdminController@importSubreddits');
});


Route::prefix('info')->middleware('auth')->group(function() {
    Route::get('subreddits', 'InfoController@indexSubreddits');
    
    Route::get('autocomplete', 'SearchController@autocomplete');
    
    Route::prefix('tags')->group(function() {
        Route::get('/', 'InfoController@indexTags');
        Route::get('namespaces', 'InfoController@indexTagNamespaces');
        
        Route::prefix('suggestions')->group(function() {
            Route::post('/', 'TagSuggestionController@store');
            
            Route::get('top', 'TagSuggestionController@indexTop');
            Route::get('recent', 'TagSuggestionController@indexRecent');
            Route::get('search', 'TagSuggestionController@indexSearch');
            Route::get('mine', 'TagSuggestionController@indexMine');
                
            Route::get('{tag_suggestion}', 'TagSuggestionController@view');
            Route::post('{tag_suggestion}/vote', 'TagSuggestionController@vote');
        });
    });
});