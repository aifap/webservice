let alertStore = {
    state: {
        message: null,
        open:  false,
        timeoutHandle: null,
    },
    
    showAlert(message, timeout = 3000) {
        this.state.message = message;
        this.state.open = true;
        if (this.state.timeoutHandle) {
            clearTimeout(this.state.timeoutHandle);
        }
        this.state.timeoutHandle = setTimeout(() => {
            this.state.open = false;
        }, timeout);
    },
    
};

export default alertStore;
