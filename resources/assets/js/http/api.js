import axios from 'axios';
import localStorage from 'local-storage-fallback';
import qs from 'qs';
import alert from '@/services/alert';

let api = axios.create({
    baseURL: '/api/v1/',
    timeout: 60000,
    paramsSerializer: params => qs.stringify(params, { arrayFormat: 'brackets' })
});

api.interceptors.response.use(
  config => config,
  (error) => {
      console.log('response intercepted', {error});
    if (error.response.status === 401) {
        alert.showAlert('Session expired. Refreshing...');
        setTimeout(() => {
            window.location.reload();
        }, 2000);
    }
    return Promise.reject(error);
  },
);

export default api;