import AboutIndex from './AboutIndex';
import AboutUs from './AboutUs';

export default [
    {path: '/about', component: AboutIndex, children: [
        {path: '/', name: 'AboutUs', component: AboutUs, meta: {title: 'About'}},
        
        
        // Redirects:
        {path: 'tags', name: 'AboutTags', redirect: {name: 'ContentTags'}},
        {path: 'tags/suggest', children: [
            {path: '/', name: 'AboutSuggestTagsTop', redirect: {name: 'ContentSuggestTagsTop'}},
            {path: 'recent', name: 'AboutSuggestTagsRecent', redirect: {name: 'ContentSuggestTagsRecent'}},
            {path: 'search', name: 'AboutSuggestTagsSearch', redirect: {name: 'ContentSuggestTagsSearch'}},
            {path: 'new', name: 'AboutSuggestTagsNew', redirect: {name: 'ContentSuggestTagsNew'}},
            {path: 'info', name: 'AboutSuggestTagsInfo', redirect: {name: 'ContentSuggestTagsInfo'}},
            {path: 'my-suggestions', name: 'AboutSuggestTagsMine', redirect: {name: 'ContentSuggestTagsMine'}},
        ]},
        {path: 'subreddits', name: 'AboutSubreddits', redirect: {name: 'ContentSubreddits'}},
    ]},
];