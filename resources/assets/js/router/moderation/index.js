import ModIndex from './ModIndex';
import ModSubreddits from './ModSubreddits';
import ModTags from './ModTags';
import ModTaggings from './ModTaggings';
import ModAmends from './ModAmends';

export default [
    {path: '/moderation', name: 'ModIndex', component: ModIndex, children: [
        {path: 'subreddits', name: 'ModSubreddits', component: ModSubreddits, meta: {title: 'Mod subreddits', requiresAuth: true}},
        {path: 'tags', name: 'ModTags', component: ModTags, meta: {title: 'Mod tags', requiresAuth: true}},
        {path: 'taggings', name: 'ModTaggings', component: ModTaggings, meta: {title: 'Mod taggings', requiresAuth: true}},
        {path: 'amends', name: 'ModAmends', component: ModAmends, meta: {title: 'Mod amends', requiresAuth: true}},
    ]},
];