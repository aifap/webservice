Toggle V2 rec strategies:
    The V2 rec feed now includes some advanced options which allow
    you to toggle which recommendation strategies are included
    in your feed.