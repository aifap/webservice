Better list management:
    Added a new dropdown to posts in the feed view, with an option to
        add a post directly to a list.
    In the 'add to list' popup, you can now see which lists a post is already in,
        add the post to multiple lists, and remove it from lists.