Community tag management:
    You can now add and remove tags from posts. You'll need a certain level of
    Trust to start tagging.
Trust System:
    There's now a new reputation metric named 'Trust'. You can view your Trust 
    in your profile. Higher Trust levels will unlock new moderation abilities 
    and tools. Increase your Trust by amending and tagging posts.
