<!DOCTYPE html>
<html>
<head>
    <title>AIFap - Maintenance</title>
    <style>
    body,
    html {
        margin: 0;
        padding: 0;
        background: #111;
        display: flex;
        justify-content: center;
        align-items: center;
        
        color: white;
        font-family: 'Arial', sans-serif;
    }
    
    .AIF-Maintenance {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        width: 300px;
        height: 300px;
    }
    
    .AIF-Maintenance img {
        height: 80px;
        margin-bottom: 20px;
    }
    </style>
</head>
<body>
    <div class="AIF-Maintenance">
        <img src="/fonts/brand.svg">
        <span>
            Deploying updates! Be right back!
        </span>
    </div>
</body>
</html>